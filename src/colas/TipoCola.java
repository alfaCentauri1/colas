package colas;

public interface TipoCola {
    void agregar(int dato);
    Nodo extraer();
    boolean empty();
    int delete();
    int inicio();
    int size();
}
