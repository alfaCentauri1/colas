package colas;

public class Cola implements TipoCola{
    private Nodo primero;
    private Nodo ultimo;
    private int tamanio;

    public Cola() {
        primero = null;
        ultimo = null;
        tamanio = 0;
    }

    @Override
    public void agregar(int dato) {
        Nodo nuevo = new Nodo(dato);
        if (empty()){
            primero = nuevo;
        }
        else{
            ultimo.setSiguiente(nuevo);
        }
        ultimo = nuevo;
        tamanio++;
    }

    @Override
    public Nodo extraer() {
        Nodo encontrado = primero;
        primero = primero.getSiguiente();
        tamanio--;
        return encontrado;
    }

    @Override
    public boolean empty() {
        return (primero == null);
    }

    @Override
    public int delete() {
        int encontrado = primero.getDato();
        primero = primero.getSiguiente();
        tamanio--;
        return encontrado;
    }

    @Override
    public int inicio() {
        return primero.getDato();
    }

    @Override
    public int size() {
        return tamanio;
    }

    public void imprimir(){
        System.out.println("La Colas es: ");
        while (!empty()){
            System.out.println(extraer().getDato());
        }
    }
}
